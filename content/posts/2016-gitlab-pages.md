Title: Pelican on GitLab Pages and Windows!
Date: 2016-05-28
Category: GitLab
Tags: pelican, gitlab
Slug: pelican-on-gitlab-pages

This site is hosted on GitLab Pages!

This is fork of pelican gitlab pages project. You can find sources
at <https://gitlab.com/alekc/pelican>. In the repository you will
find additional scripts for running Pelican under __Windows__ easier.

The original source code of this site is at <https://gitlab.com/pages/pelican>.


Learn about GitLab Pages at <https://pages.gitlab.io>.
